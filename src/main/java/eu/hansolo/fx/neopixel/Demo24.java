package eu.hansolo.fx.neopixel;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;


/**
 * User: hansolo
 * Date: 29.04.14
 * Time: 16:33
 */
public class Demo24 extends Application {    
    private NeoPixel24     neoPixel;
    private long           interval;
    private long           lastTimerCall;
    private AnimationTimer timer;
    private double         tmpValue;
    private double         value;
    private double         minValue;
    private double         maxValue;
    private double         range;
    private GradientLookup gradientLookup;
    private double         ledStepSize;


    @Override public void init() {
        tmpValue = -15;
        value = 0;
        minValue = -15;
        maxValue = 40;
        range = maxValue - minValue;
        ledStepSize = range / 19;

        neoPixel = new NeoPixel24();
        interval = 0_250_000_000l;
        lastTimerCall = System.nanoTime();
        timer = new AnimationTimer() {
            @Override public void handle(long now) {
                if (now > lastTimerCall + interval) {
                    tmpValue++;
                    if (tmpValue > maxValue) tmpValue = minValue;
                    setValue(tmpValue);
                    lastTimerCall = now;
                }
            }
        };
        gradientLookup = new GradientLookup(new Stop(0.00, Color.rgb(0, 0, 255)),
                                            new Stop(0.25, Color.rgb(0, 255, 255)),
                                            new Stop(0.50, Color.rgb(  0, 255,   0)),
                                            new Stop(0.75, Color.rgb(255, 255,   0)),
                                            new Stop(1.00, Color.rgb(255,   0,   0)));
    }
    
    private void setValue(final double VALUE) {
        value          = clamp(minValue, maxValue, VALUE);
        int noOfLeds   = (int) Math.round((value - minValue) / ledStepSize);
        Color ledColor = gradientLookup.getColorAt((value - minValue) / range);
        neoPixel.setAllLedsOff();
        for (int i = 0 ; i <= noOfLeds ; i++) {
            if (i > 9) {
                neoPixel.setLedColor(i - 10, ledColor);
            } else {
                neoPixel.setLedColor(15 + i, ledColor);
            }
        }        
    }

    private double clamp(final double MIN, final double MAX, final double VALUE) {
        if (VALUE < MIN) return MIN;
        if (VALUE > MAX) return MAX;
        return VALUE;
    }

    @Override public void start(Stage stage) throws InterruptedException {
        StackPane pane = new StackPane();
        pane.setBackground(new Background(new BackgroundFill(Color.rgb(80, 80, 80), CornerRadii.EMPTY, Insets.EMPTY)));
        pane.getChildren().addAll(neoPixel);

        Scene scene = new Scene(pane);

        stage.setTitle("NeoPixel24");
        stage.setScene(scene);
        stage.show();

        timer.start();
    }

    @Override public void stop() {        
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
