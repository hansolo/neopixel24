package eu.hansolo.fx.neopixel;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.FillRule;

import java.util.ArrayList;
import java.util.List;


/**
 * User: hansolo
 * Date: 25.04.14
 * Time: 11:06
 */
public class NeoPixel24 extends Region {
    private static final double PREFERRED_WIDTH  = 650;
    private static final double PREFERRED_HEIGHT = 650;
    private static final double MINIMUM_WIDTH    = 65;
    private static final double MINIMUM_HEIGHT   = 65;
    private static final double MAXIMUM_WIDTH    = 1024;
    private static final double MAXIMUM_HEIGHT   = 1024;
    private double                   size;
    private double                   width;
    private double                   height;
    private Pane                     pane;
    private Canvas                   ringCanvas;
    private GraphicsContext          ringCtx;
    private Canvas                   ledCanvas;
    private GraphicsContext          ledCtx;
    private ObservableList<Color>    ledColors;
    private List<DropShadow>         ledGlows;
    private Point2D                  center;


    // ******************** Constructors **************************************
    public NeoPixel24() {
        init();
        initGraphics();
        registerListeners();
    }


    // ******************** Initialization ************************************
    private void init() {
        if (Double.compare(getPrefWidth(), 0.0) <= 0 || Double.compare(getPrefHeight(), 0.0) <= 0 ||
            Double.compare(getWidth(), 0.0) <= 0 || Double.compare(getHeight(), 0.0) <= 0) {
            if (getPrefWidth() > 0 && getPrefHeight() > 0) {
                setPrefSize(getPrefWidth(), getPrefHeight());
            } else {
                setPrefSize(PREFERRED_WIDTH, PREFERRED_HEIGHT);
            }
        }

        if (Double.compare(getMinWidth(), 0.0) <= 0 || Double.compare(getMinHeight(), 0.0) <= 0) {
            setMinSize(MINIMUM_WIDTH, MINIMUM_HEIGHT);
        }

        if (Double.compare(getMaxWidth(), 0.0) <= 0 || Double.compare(getMaxHeight(), 0.0) <= 0) {
            setMaxSize(MAXIMUM_WIDTH, MAXIMUM_HEIGHT);
        }
    }

    private void initGraphics() {
        center    = new Point2D(0.5 * PREFERRED_WIDTH, 0.5 * PREFERRED_HEIGHT);
        ledColors = FXCollections.observableArrayList();
        ledGlows  = new ArrayList<>(24);
        for (int i = 0 ; i < 24 ; i++) { 
            ledColors.add(Color.TRANSPARENT);
            ledGlows.add(new DropShadow(BlurType.TWO_PASS_BOX, Color.TRANSPARENT, 0.03 * PREFERRED_WIDTH, 0, 0, 0));
        }
        
        ringCanvas = new Canvas(PREFERRED_WIDTH, PREFERRED_HEIGHT);
        ringCtx    = ringCanvas.getGraphicsContext2D();
        
        ledCanvas  = new Canvas(PREFERRED_WIDTH, PREFERRED_HEIGHT);
        ledCtx     = ledCanvas.getGraphicsContext2D();
                
        pane = new Pane();
        pane.getChildren().addAll(ringCanvas, ledCanvas);
        
        getChildren().setAll(pane);
        resize();
    }

    private void registerListeners() {
        widthProperty().addListener(observable -> resize());
        heightProperty().addListener(observable -> resize());
        ledColors.addListener((ListChangeListener<Color>) c -> drawLeds());        
    }


    // ******************** Methods *******************************************
    public final void setLedColor(final int LED, final Color LED_COLOR) {        
        ledGlows.get(clamp(0, 23, LED)).setColor(LED_COLOR);
        ledColors.set(clamp(0, 23, LED), LED_COLOR);        
    }
    public final Color getLedColor(final int LED) {
        return (ledColors.get(clamp(0, 23, LED)));
    }
    
    public final void setLedOff(final int LED) {
        setLedColor(LED, Color.TRANSPARENT);                
    }
    public final void setAllLedsOff() {
        List<Color> tmp = new ArrayList<>(24);
        for (int i = 0 ; i < 24 ; i++) {
            tmp.add(Color.TRANSPARENT);
        }
        ledColors.setAll(tmp);
    }
    
    private int clamp(final int MIN, final int MAX, final int VALUE) {
        if (VALUE < MIN) return MIN;
        if (VALUE > MAX) return MAX;
        return VALUE;
    }

    private void drawLedRing() {                
        ringCtx.clearRect(0, 0, size, size);
        ringCtx.setFillRule(FillRule.EVEN_ODD);
        ringCtx.beginPath();
        ringCtx.moveTo(0.09230769230769231 * size, center.getX());
        ringCtx.bezierCurveTo(0.09230769230769231 * size, 0.2753846153846154 * size, 0.2753846153846154 * size, 0.09230769230769231 * size, center.getX(), 0.09230769230769231 * size);
        ringCtx.bezierCurveTo(0.7246153846153847 * size, 0.09230769230769231 * size, 0.9076923076923077 * size, 0.2753846153846154 * size, 0.9076923076923077 * size, center.getX());
        ringCtx.bezierCurveTo(0.9076923076923077 * size, 0.7246153846153847 * size, 0.7246153846153847 * size, 0.9076923076923077 * size, center.getX(), 0.9076923076923077 * size);
        ringCtx.bezierCurveTo(0.2753846153846154 * size, 0.9076923076923077 * size, 0.09230769230769231 * size, 0.7246153846153847 * size, 0.09230769230769231 * size, center.getX());
        ringCtx.closePath();
        ringCtx.moveTo(0, center.getX());
        ringCtx.bezierCurveTo(0, 0.7753846153846153 * size, 0.2246153846153846 * size, size, center.getX(), size);
        ringCtx.bezierCurveTo(0.7753846153846153 * size, size, size, 0.7753846153846153 * size, size, center.getX());
        ringCtx.bezierCurveTo(size, 0.2246153846153846 * size, 0.7753846153846153 * size, 0, center.getX(), 0);
        ringCtx.bezierCurveTo(0.2246153846153846 * size, 0, 0, 0.2246153846153846 * size, 0, center.getX());
        ringCtx.closePath();
        ringCtx.setFill(Color.rgb(17, 17, 17));
        ringCtx.fill();

        ringCtx.setFill(Color.rgb(200, 200, 200));                           
        double  frameSize         = size * 0.07692;
        double  frameCornerRadius = 0.00615 * size;
        for (int i = 0 ; i < 24 ; i++) {                                                
            ringCtx.fillRoundRect((size - frameSize) * 0.5, size * 0.00769, frameSize, frameSize, frameCornerRadius, frameCornerRadius);                        
            ringCtx.translate(center.getX(), center.getY());
            ringCtx.rotate(15);
            ringCtx.translate(-center.getX(), -center.getY());
        }
    }
    
    private void drawLeds() {
        ledCtx.clearRect(0, 0, size, size);
        ledCtx.setStroke(Color.WHITE);                
        double  ledSize = size * 0.06154;
        for (int i = 0 ; i < 24 ; i++) {            
            ledCtx.save();            
            ledCtx.setEffect(ledGlows.get(i));            
            ledCtx.setFill(new RadialGradient(0, 0, 
                                              center.getX(), 0.01538 * size + 0.5 * ledSize,
                                              0.5 * ledSize,
                                              false, CycleMethod.NO_CYCLE,
                                              new Stop(0.0, ledColors.get(i).deriveColor(0, 0.3, 1.1, 1)),
                                              new Stop(0.75, ledColors.get(i)),
                                              new Stop(1.0, ledColors.get(i).deriveColor(0, 1, 1.3, 1))));                        
            ledCtx.fillOval((size - ledSize) * 0.5, size * 0.01538, ledSize, ledSize);
            ledCtx.restore();
            ledCtx.strokeOval((size - ledSize) * 0.5, size * 0.01538, ledSize, ledSize);
            ledCtx.translate(center.getX(), center.getY());
            ledCtx.rotate(15);
            ledCtx.translate(-center.getX(), -center.getY());
        }        
    }
            
    
    // ******************** Resizing ******************************************
    private void resize() {        
        width  = getWidth();
        height = getHeight();
        size   = width < height ? width : height;
        
        if (width > 0 && height > 0) {
            center = new Point2D(size * 0.5, size * 0.5);
            
            for (DropShadow glow : ledGlows) {
                glow.setRadius(0.04 * size);
            }
            
            pane.setMaxSize(size, size);
            pane.relocate((width - size) * 0.5, (height - size) * 0.5);
            
            ringCanvas.setWidth(size);
            ringCanvas.setHeight(size);
            
            ledCanvas.setWidth(size);
            ledCanvas.setHeight(size);
            
            drawLedRing();
            drawLeds();
        }
    }
}
