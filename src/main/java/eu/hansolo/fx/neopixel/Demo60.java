package eu.hansolo.fx.neopixel;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.time.LocalTime;


/**
 * User: hansolo
 * Date: 06.05.14
 * Time: 07:56
 */
public class Demo60 extends Application {
    private static final Color NUMBER_COLOR = Color.LIME;
    private static final Color HOUR_COLOR   = Color.BLUE;
    private static final Color MINUTE_COLOR = Color.CYAN;
    private static final Color SECOND_COLOR = Color.RED;
    private NeoPixel60     neoPixel;
    private int            lastHour;
    private int            lastMinute;
    private int            lastSecond;
    private long           interval;
    private long           lastTimerCall;
    private AnimationTimer timer;
    


    @Override public void init() {        
        neoPixel      = new NeoPixel60();
        lastHour      = (LocalTime.now().getHour() % 12) * 5;
        lastMinute    = LocalTime.now().getMinute();
        lastSecond    = LocalTime.now().getSecond(); 
        interval      = 1_000_000_000l;
        lastTimerCall = System.nanoTime();
        timer = new AnimationTimer() {
            @Override public void handle(long now) {
                if (now > lastTimerCall + interval) {
                    setTime(LocalTime.now());
                    lastTimerCall = now;
                }
            }
        };        
        initClock();   
    }

    private void initClock() {                         
        neoPixel.setAllLedsOff();
        for (int i = 0 ; i < 60 ; i++) {
            if (i % 5 == 0) {
                neoPixel.setLedColor(i, NUMBER_COLOR);
            }            
        }
    }
    
    private void setTime(final LocalTime TIME) {                
        int h = (TIME.getHour() % 12) * 5;
        int m = TIME.getMinute();
        int s = TIME.getSecond();

        if (h != lastHour)   neoPixel.setLedColor(lastHour, Color.TRANSPARENT);
        if (m != lastMinute) neoPixel.setLedColor(lastMinute, Color.TRANSPARENT);
        if (s != lastSecond) neoPixel.setLedColor(lastSecond, Color.TRANSPARENT);
        
        if (lastHour   % 5 == 0) neoPixel.setLedColor(lastHour, NUMBER_COLOR);
        if (lastMinute % 5 == 0) neoPixel.setLedColor(lastMinute, NUMBER_COLOR);
        if (lastSecond % 5 == 0) neoPixel.setLedColor(lastSecond, NUMBER_COLOR);
        
        neoPixel.setLedColor(h, HOUR_COLOR);
        neoPixel.setLedColor(m, MINUTE_COLOR);
        neoPixel.setLedColor(s, SECOND_COLOR);
        
        lastHour   = h;
        lastMinute = m;
        lastSecond = s;
    }
            
    @Override public void start(Stage stage) throws InterruptedException {
        StackPane pane = new StackPane();
        pane.setBackground(new Background(new BackgroundFill(Color.rgb(80, 80, 80), CornerRadii.EMPTY, Insets.EMPTY)));
        pane.getChildren().addAll(neoPixel);

        Scene scene = new Scene(pane);

        stage.setTitle("NeoPixel60");
        stage.setScene(scene);
        stage.show();

        timer.start();
    }

    @Override public void stop() {
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
